package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@Tag("unit")
@SpringBootTest
class KaamelottQuoteApplicationTests {

    @Test
    fun contextLoads() {
    }
}
